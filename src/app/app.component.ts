import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'calculator';

  firstOperator = 0.0;
  lastOperator = 0.0;
  isFirstOperator = false;

  pressNumber(value: number){
    console.log(value)
    if(this.isFirstOperator){
      this.firstOperator = value;
    } else {
      this.lastOperator = value;
    }
  }

  pressOperator(opeator: string){}
}

